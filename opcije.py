

import funkcije


def kupac():
	print('kupac')
	odg = '123'
	while odg not in ['1','2','3','4','5','6','x']:
		odg = input("Izaberite jednu od sledecih opcija\n\
	 1. Prikaz dostupnih filmova\n \
	 2. Pretraga filmova\n \
	 3. Pretraga projekacija\n\
	 4. Rezervacija karata\n\
	 5. Pregled rezervisanih karata \n\
	 6. Brisanje rezervisanih karata \n\
	 x. x za izlaz \n\
	Vas odgovor: ")
	if odg == '1':
		funkcije.dostupni()
		print('1')
		kupac()
	elif odg == '2':
		funkcije.pretraga_filmova()
		print('2')
		kupac()		
	elif odg == '3':
		funkcije.pretraga_projekcija()
		print('3')
		kupac()
	elif odg == '4':
		print('rezervacija: ')
		funkcije.rezervacija()
		kupac()
	elif odg == '5':
		print('pregled rezervisanih karata')
		funkcije.pregled()
		kupac()
	elif odg == '6':
		print('brisanje rezervisanih karata')
		funkcije.brisanje()
		kupac()
	elif odg == 'x':
		exit()	
def prodavac():
	print('prodavac')
	odg = '123'
	while odg not in ['1','2','3','4','5','6','7','x']:
		odg = input("Izaberite jednu od sledecih opcija\n\
	 1. Prikaz dostupnih filmova\n \
	 2. Pretraga filmova\n \
	 3. Pretraga projekacija\n\
	 4. Rezervacija karata\n\
	 5. Pregled rezervisanih karata \n\
	 6. Prodaja karata \n\
	 7. Brisanje karata \n\
	 x. x za izlaz \n\
	Vas odgovor:  ")
	if odg == '1':
		print('dostupni filmovi')
		funkcije.dostupni()
		prodavac()
	elif odg == '2':
		print('pretraga filmova')
		funkcije.pretraga_filmova()
		prodavac()		
	elif odg == '3':
		print('pretraga projekcija')
		funkcije.pretraga_projekcija()
		prodavac()
	elif odg == '4':
		print('rezervacija karata')
		funkcije.rezervacija_karata()
		prodavac()
	elif odg == '5':
		print('sve rezervisane karte')
		funkcije.karte()
		prodavac()
	elif odg == '6':
		print('prodaja karata')
		funkcije.prodaja()
		prodavac()
	elif odg == '7':
		print('brisanje karata')
		funkcije.brisanje_karata()
		prodavac()	
	elif odg == 'x':
		exit()	

def menadzer():
	print('menadzer')
	odg = '123'
	while odg not in ['1','2','3','4','5','6','7','8','9','10','x']:
		odg = input("Izaberite jednu od sledecih opcija\n\
	 1. Prikaz dostupnih filmova\n \
	 2. Pretraga filmova\n \
	 3. Pretraga projekacija\n \
	 4. Upis novih korisnika \n \
	 5. Upis novih filmova\n \
	 6. Upis projekacija \n\
	 7. Upis sali za projekcije:\n\
	 8. Upis termina za projekciju filma \n\
	 9. Prodaja karata \n\
	 10. Izvestaj \n\
	 x. x za izlaz \n\
	 Vas odgovor:  ")

	if odg == '1':
		print('dostupni filmovi')
		funkcije.dostupni()
		menadzer()
	elif odg == '2':
		print('Pretraga filmova')
		funkcije.pretraga_filmova()
		print('2')
		menadzer()		
	elif odg == '3':
		print('Pretraga projekcija')
		funkcije.pretraga_projekcija()
		print('3')
		menadzer()
	elif odg == '4':
		print('Upis novih korisnika')
		funkcije.novi_prodavac()
		menadzer()
	elif odg == '5':
		print('upis novih filmova')
		funkcije.novi_film()
		menadzer()				
	elif odg == '6':
		print('Upis novih projekcija')
		funkcije.bioskopske_projekcije()	
	elif odg == '7':
		print('Upis sali za projekcije')
		funkcije.nove_sale()
		menadzer()
	elif odg == '8':
		print('Novi termini')
		funkcije.termin()
		menadzer()		
	elif odg == '9':
		print('Prodaja karata')
		funkcije.prodaja()
		menadzer()
	elif odg == '10':
		print('Izvestaj')
		funkcije.izvestaj()
		menadzer()		
	elif odg == 'x':
		exit()			